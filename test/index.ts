import { expect } from 'chai';
import Fetcher from '../index'

describe('Fetcher', () => {
    it('can be initialized without an initializer', () => {
        const f = new Fetcher();
        expect(f).a.instanceOf(Fetcher);
    });
});
