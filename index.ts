import axios, { AxiosPromise, AxiosInstance } from 'axios'

declare interface FetcherConfig {
    baseURL?: string;
}

declare interface FetcherReqConfig {
    id?: string,
    data?: Object,
    params?: Object,
    del?: boolean;
}

export default class Fetcher {
    private _axios: AxiosInstance;

    constructor(options: FetcherConfig = {}) {
        let o: FetcherConfig = Object.assign({
            baseURL: '/api/'
        }, options)
        let headers = { 'X-Requested-With': 'XMLHttpRequest' };
        try {
            let token: any = document.head.querySelector('meta[name="csrf-token"]');
            headers['X-CSRF-TOKEN'] = token.content;
        } catch (e) { }
        this._axios = axios.create({
            baseURL: o.baseURL, headers
        });

        let self = this;
        [
            'users',
            'articles',
            'boxes',
            'sports_hall',
            'orders',
            'faq',
            'discounts',
            'credits',
            'delivery_type'
        ].forEach(t => {
            Fetcher.prototype[t] = (args?: FetcherReqConfig): AxiosPromise => self.req(t, args);
        });

        ['discounts'].forEach(t => {
            Fetcher.prototype[t].search = (params?: Object): AxiosPromise => self.search(t, params);
        });
    }

    req(type: string, args?: FetcherReqConfig): AxiosPromise {
        let o = Object.assign({
            id: '',
            data: {},
            params: {},
            del: false
        }, args);
        if (o.del) {
            return this._axios.delete(`${type}/${o.id}`, { params: o.params });
        } else if (o.id.length === 0 && Object.keys(o.data).length === 0) {
            return this._axios.get(type, { params: o.params });
        } else if (o.id.length === 0 && Object.keys(o.data).length !== 0) {
            return this._axios.post(type, o.data, { params: o.params });
        } else if (o.id.length !== 0 && Object.keys(o.data).length === 0) {
            return this._axios.get(`${type}/${o.id}`, { params: o.params });
        } else if (o.id.length !== 0 && Object.keys(o.data).length !== 0) {
            return this._axios.put(`${type}/${o.id}`, o.data, { params: o.params });
        }
        throw new Error('impossibru');
    }

    search(type: string, params: Object): AxiosPromise {
        return this._axios.get(`${type}/search`, { params });
    }

    get user(): AxiosPromise {
        return this._axios.get('user');
    }

    get axios(): AxiosInstance {
        return this._axios;
    }

    static install(Vue: any, options: FetcherConfig = {}): void {
        Vue.prototype.$http = new Fetcher(options);
    }
}
