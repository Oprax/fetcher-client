import { AxiosPromise, AxiosInstance } from 'axios';

export interface FetcherConfig {
    baseURL?: string;
}

export interface FetcherReqConfig {
    id?: string,
    data?: Object,
    params?: Object,
    del?: boolean;
}

export interface FetcherInstance {
    (options: FetcherConfig): void;
    req(type: string, args?: FetcherReqConfig): AxiosPromise;
    search(type: string, params: Object): AxiosPromise;
    user: AxiosPromise;
    axios: AxiosInstance;
    install(Vue: any, options: FetcherConfig): void;
}

declare const Fetcher: FetcherInstance;

export default Fetcher;
